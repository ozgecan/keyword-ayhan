# -*- coding: utf-8 -*-
"""
Created on Thu Jul 14 09:05:08 2016

@author: ayhanyavuz
"""

from flask import Flask, render_template
#from flask_wtf import Form
from forms import ContactForm

app = Flask(__name__)
app.secret_key = 'development key'

@app.route('/contact')
def contact():
   form = ContactForm()
   return render_template('contact.html', form = form)

if __name__ == '__main__':
   app.run(debug = True)