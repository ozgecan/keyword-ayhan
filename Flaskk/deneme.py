# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 16:24:33 2016

@author: ayhanyavuz
"""

from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'This is a home page.'


if __name__ == "__main__":
    app.run()    