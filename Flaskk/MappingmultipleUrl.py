# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 14:48:37 2016

@author: ayhanyavuz
"""

from flask import Flask , render_template

app = Flask(__name__)

@app.route("/")
@app.route("/<user>")
def index(user=None):
    return render_template("user.html", user=user)
if __name__ == "__main__":
    app.run()   