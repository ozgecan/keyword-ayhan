# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 09:17:03 2016

@author: ayhanyavuz
"""
import flask, flask.views
from flask import request
from werkzeug import secure_filename
import nltk, Zemberek
import string, os, io
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from snowballstemmer import TurkishStemmer
import networkx as nx
import itertools
import tf_idf

app = flask.Flask(__name__)

path=''
token_dict = {}

def filter_for_tags(tagged, tags=['NN', 'JJ', 'NNP']):
    return [item for item in tagged if item[1] in tags]
    
def normalize(tagged):
    return [(item[0].replace('.', ''), item[1]) for item in tagged]

def unique_everseen(iterable, key=None):
    "List unique elements, preserving order. Remember all elements ever seen."
    # unique_everseen('AAAABBBCCDAABBB') --> A B C D
    # unique_everseen('ABBCcAD', str.lower) --> A B C D
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in itertools.ifilterfalse(seen.__contains__, iterable):
            seen_add(element)
            yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield element
                
def lDistance(firstString, secondString):

    if len(firstString) > len(secondString):
        firstString, secondString = secondString, firstString
    distances = range(len(firstString) + 1)
    for index2, char2 in enumerate(secondString):
        newDistances = [index2 + 1]
        for index1, char1 in enumerate(firstString):
            if char1 == char2:
                newDistances.append(distances[index1])
            else:
                newDistances.append(1 + min((distances[index1], distances[index1+1], newDistances[-1])))
        distances = newDistances
    return distances[-1]

def buildGraph(nodes):
    "nodes - list of hashables that represents the nodes of the graph"
    gr = nx.Graph() #initialize an undirected graph
    gr.add_nodes_from(nodes)
    nodePairs = list(itertools.combinations(nodes, 2))
    #add edges to the graph (weighted by Levenshtein distance)  
    for pair in nodePairs:
        firstString = pair[0]
        secondString = pair[1]
        levDistance = lDistance(firstString, secondString)
        gr.add_edge(firstString, secondString, weight=levDistance)
               
    return gr

def tokenize(text):
    
    language='turkish'
    stopwords_= stopwords.words(language) 
    digit = ['0','1','2','3','4','5','6','7','8','9']
    stopwords_.extend(["bir","bu","iki","üç".decode("utf8"),"’", "’de", "’da"]) 
   
    tokens= nltk.word_tokenize(text, language=language)
    tokens=list(set(tokens)-set(stopwords_)-set(digit))
    stems = []
    for item in tokens:
        stems.append(TurkishStemmer().stemWord(item))
#    stems = sorted(stems,reverse=True) 
    
    return stems

for dirpath, dirs, files in os.walk(path):
    for f in files:
        fname = os.path.join(dirpath, f)
        print "fname=", fname
        with open(fname) as data:
            text = data.read()
            token_dict[f] = text.lower().translate(None, string.punctuation).decode("utf8")

def writeFiles(keyphrases, fileName):
    "outputs the keyphrases to appropriate files"
    print "Generating output to " + 'keywords/' + fileName
    keyphraseFile = io.open('keywords/' + fileName, 'w',encoding='utf8')
    for keyphrase in keyphrases:
        keyphraseFile.write(keyphrase + '\n')
    keyphraseFile.close()

@app.route('/', methods=['GET'])
def index():
    return flask.render_template('index.html', modifiedKeyphrases="")
@app.route('/', methods=['GET', 'POST'])
def openFile():
    mainDirname=os.path.dirname(__file__)
    
    if request.method == 'POST':
        f = request.files['file']
        filePath=os.path.join(mainDirname,"data",secure_filename(f.filename))
        f.save(filePath)
        articleFile=open(filePath,'r')
        text=articleFile.read()
        articleFile.close()
        
        modifiedKeyphrases=tf_idf.tf_idf_model(f.filename, text)
    
    return flask.render_template('index.html', modifiedKeyphrases=modifiedKeyphrases) 

if __name__ == "__main__":
    app.debug = True
    app.run(port=5000)       