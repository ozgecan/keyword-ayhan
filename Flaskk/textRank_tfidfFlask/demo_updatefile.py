from flask import Flask, render_template, request
from werkzeug import secure_filename

app = Flask(__name__)

@app.route('/')
#@app.route('/upload')
def upload_file():
   return render_template('upload.html')
	
@app.route('/', methods = ['GET', 'POST'])
def index():
   if request.method == 'POST':
      f = request.files['file']
#      f.save(secure_filename(f.filename))
      articleFile=open(secure_filename(f.filename),'r')
      text=articleFile.read()
      articleFile.close()
      
      return render_template('upload.html')
		
if __name__ == '__main__':
    app.run(debug = True)