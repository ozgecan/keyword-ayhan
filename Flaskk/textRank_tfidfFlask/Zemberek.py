#!/usr/bin/python 
# -*- coding: utf-8 -*-


import os

d=os.path.dirname(__file__) 
os.environ['JAVA_HOME']="/usr/lib/jvm/java-8-oracle" 
os.environ["CLASSPATH"] = os.path.join(d,"src","zemberek-tum-2.0.jar")
os.environ.update()
 
from jnius import autoclass
  
#java kutuphaneleri

import sys
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('utf8')
zemberek_class=autoclass('net.zemberek.erisim.Zemberek') #Javadan zemberek sinifi cagriliyor
turkiye_turkcesi=autoclass('net.zemberek.tr.yapi.TurkiyeTurkcesi') #Turkce dili yukleniyor
KelimeTipi=autoclass('net.zemberek.yapi.KelimeTipi')
 
JList=autoclass('java.util.List')

ZEMBEREK=zemberek_class(turkiye_turkcesi())

def tr_pos_tag(wordList):
    pos_tag_list=[]
    for i in range (0, len(wordList)):         
         if ZEMBEREK.kelimeDenetle(wordList[i]):
             sonuc = ZEMBEREK.kelimeCozumle(wordList[i])
             kelime = sonuc[0]
             tag=""
             if   kelime.kok().tip().toString().lower() == "fiil":
                 tag="VB"
             elif kelime.kok().tip().toString().lower() == "isim":
                 tag="NN"
             elif kelime.kok().tip().toString().lower() == "zamir":
                 tag="PRP" 
             elif kelime.kok().tip().toString().lower() == "sifat":
                 tag="JJ"    
             elif kelime.kok().tip().toString().lower() == "zarf":
                 tag="RB"     
             elif kelime.kok().tip().toString().lower() == "edat":
                 tag="IN"    
             elif kelime.kok().tip().toString().lower() == "baglac":
                 tag="CC"
             elif kelime.kok().tip().toString().lower() == "unlem":
                 tag="UH"
             pos_tag_list.append((str(kelime.kok().icerik()).decode("utf8"), 
                                  tag))

    return pos_tag_list