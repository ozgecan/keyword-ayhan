# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 14:02:13 2016

@author: ayhanyavuz
"""

from flask import Flask , render_template

app = Flask(__name__)

@app.route("/profile/<name>")
def profile(name):
    return render_template("profile.html", name=name)


if __name__ == "__main__":
    app.run()   