# -*- coding: utf-8 -*-
"""
Created on Tue Jun 28 13:19:20 2016

@author: ayhanyavuz
"""

import nltk ,Zemberek,io, string, os
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from snowballstemmer import TurkishStemmer
import networkx as nx
import itertools
from langdetect import detect 


import sys
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('utf8')


path = 'articles'
token_dict = {}
  
def filter_for_tags(tagged, tags=['NN', 'JJ', 'NNP']):
    return [item for item in tagged if item[1] in tags]
    
def normalize(tagged):
    return [(item[0].replace('.', ''), item[1]) for item in tagged]
   
def lDistance(firstString, secondString):

    if len(firstString) > len(secondString):
        firstString, secondString = secondString, firstString
    distances = range(len(firstString) + 1)
    for index2, char2 in enumerate(secondString):
        newDistances = [index2 + 1]
        for index1, char1 in enumerate(firstString):
            if char1 == char2:
                newDistances.append(distances[index1])
            else:
                newDistances.append(1 + min((distances[index1], distances[index1+1], newDistances[-1])))
        distances = newDistances
    return distances[-1]

def buildGraph(nodes):
    "nodes - list of hashables that represents the nodes of the graph"
    gr = nx.Graph() #initialize an undirected graph
    gr.add_nodes_from(nodes)
    nodePairs = list(itertools.combinations(nodes, 2))
    #add edges to the graph (weighted by Levenshtein distance)  
    for pair in nodePairs:
        firstString = pair[0]
        secondString = pair[1]
        levDistance = lDistance(firstString, secondString)
        gr.add_edge(firstString, secondString, weight=levDistance)
               
    return gr

def languadeDetect(text):
    language=detect(text.decode("utf8"))
    return language
    
def tokenizes(text):
    global lang
    #tokenize the text using nltk
    #corpus temizleme
    wordTokens = []
        
    language=""
    if lang == 'tr':
        language="turkish"
    elif lang == 'en':
        language="english"    
    stopwords_= stopwords.words(language) 
    
    if lang =='tr':
        stopwords_.extend(["bir","iki","üç".decode("utf8"),"’", "’de", "’da"])  
        
    wordTokens= nltk.word_tokenize(text, language=language) 
    
    tr_st=TurkishStemmer()
    wordTokens=tr_st.stemWords(wordTokens)
    filtered = [w for w in wordTokens if not w in stopwords_]
    return filtered

def tokenize(text):
    language='turkish'
    stopwords_= stopwords.words(language) 
    digit = ['0','1','2','3','4','5','6','7','8','9']
    stopwords_.extend(["bir","bu","iki","üç".decode("utf8"),"’", "’de", "’da"]) 
   
    tokens= nltk.word_tokenize(text, language=language)
    print tokens
    tokens=list((set(tokens)-set(stopwords_))-set(digit))
    stems = []
    for item in tokens:
        stems.append(TurkishStemmer().stemWord(item))
    stems = sorted(stems,reverse=True) 
    
    return stems
    
def extractKeyphrases(lowers, lang):    
    
#    [lang,filtered]=tokenizes(lowers)
    
    tfidf = TfidfVectorizer(tokenizer=tokenize)
    tfidf_matrix =  tfidf.fit_transform(lowers)
    feature_names = tfidf.get_feature_names() 
        
    dense = tfidf_matrix.todense()
    episode = dense[0].tolist()[0]
    phrase_scores = [pair for pair in zip(range(0, len(episode)), episode) if pair[1] > 0]
    sorted_phrase_scores = sorted(phrase_scores, key=lambda t: t[1] * -1, reverse=True)

    limit=int(len(phrase_scores)-((len(phrase_scores)*20)/100))
    filtered=[feature_names[word_id] for (word_id, score) in sorted_phrase_scores][:limit]
#    print feature_names
    #tagger olusturma
    if lang == 'tr':
        tagged=Zemberek.tr_pos_tag(filtered)
    elif lang == 'en':
        tagged = nltk.pos_tag(filtered) 
        
    #tagger temizleme
    textlist = [x[0] for x in tagged]
#    print(tagged)
    tagged = filter_for_tags(tagged)
    tagged = normalize(tagged)
    
    
    
   #this will be used to determine adjacent words in order to construct keyphrases with two words
    graph = buildGraph(filtered)

    #pageRank - initial value of 1.0, error tolerance of 0,0001, 
    calculated_page_rank = nx.pagerank(graph, weight='weight')

    #most important words in ascending order of importance
    keyphrases = sorted(calculated_page_rank, 
                        key=calculated_page_rank.get, 
                        reverse=True)

    #the number of keyphrases returned will be relative to 
    #the size of the text (a third of the number of vertices)
    aThird = len(token_dict) / 3
    keyphrases = keyphrases[0:aThird+1]
    
    #take keyphrases with multiple words into consideration 
    #as done in the paper - if two words are adjacent in the 
    #text and are selected as keywords, join them together
    modifiedKeyphrases = set([])
    
    #keeps track of individual keywords that have been joined 
    #to form a keyphrase
    dealtWith = set([]) 
    i = 0
    j = 1
    while j < len(textlist):
        firstWord = textlist[i]
        secondWord = textlist[j]
        if firstWord in keyphrases and secondWord in keyphrases:
            keyphrase = firstWord + ' ' + secondWord
            modifiedKeyphrases.add(keyphrase)
            dealtWith.add(firstWord)
            dealtWith.add(secondWord)
        else:
            if firstWord in keyphrases and firstWord not in dealtWith: 
                modifiedKeyphrases.add(firstWord)

            #if this is the last word in the text, and it is a keyword,
            #it definitely has no chance of being a keyphrase at this point    
            if j == len(textlist)-1 and secondWord in keyphrases and secondWord not in dealtWith:
                modifiedKeyphrases.add(secondWord)
        
        i = i + 1
        j = j + 1
        
    
#    print modifiedKeyphrases
    return modifiedKeyphrases   

def extractSentences(text):
    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    sentenceTokens = sent_detector.tokenize(text.strip())
    graph = buildGraph(sentenceTokens)

    calculated_page_rank = nx.pagerank(graph, weight='weight')

    #most important sentences in ascending order of importance
    sentences = sorted(calculated_page_rank, key=calculated_page_rank.get, reverse=True)

    #return a 100 word summary
    summary = ' '.join(sentences)
    summaryWords = summary.split()
    summaryWords = summaryWords[0:101]
    summary = ' '.join(summaryWords)

    return summary
    
def writeFiles(summary, keyphrases, fileName):
    "outputs the keyphrases and summaries to appropriate files"
    print "Generating output to " + 'keywords/' + fileName
    keyphraseFile = io.open('keywords/' + fileName, 'w',encoding='utf8')
    for keyphrase in keyphrases:
        keyphraseFile.write(keyphrase + '\n')
    keyphraseFile.close()

    print "Generating output to " + 'summaries/' + fileName
    summaryFile = io.open('summaries/' + fileName, 'w',encoding='utf8')
    summaryFile.write(summary)
    summaryFile.close()

#retrieve each of the articles
articles = os.listdir("articles")

for article in articles:
    print 'Reading articles/' + article
    articleFile = io.open('articles/' + article, 'r',encoding='utf8')
    text = articleFile.read()
    lowers = text.lower().decode("utf8")  
    lang=languadeDetect(lowers)
    keyphrases = extractKeyphrases(text,lang)
#    summary = extractSentences(text)
#    writeFiles(summary, keyphrases, article)
#
#articleFile = open('articles/2.txt', 'r')
#text = articleFile.read()
#lowers = text.lower().decode("utf8")  
#lang=languadeDetect(lowers)
#keyphrases = extractKeyphrases(lowers,lang)
#summary = extractSentences(lowers)
#writeFiles(summary, keyphrases, '2.txt')