# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 08:54:35 2016

@author: ayhanyavuz
"""

#!/usr/bin/python
# -*- coding: utf-8 -*-
import Zemberek
 
# [PACKAGE KURULUMU] :       pip install JPype1-py3
# [KAYNAK] : http://stackoverflow.com/questions/35736763/practical-use-of-java-class-jar-in-python
#            https://blog.notmyidea.org/using-jpype-to-bridge-python-and-java.html
#            http://iacobelli.cl/blog/?p=119
#            https://blog.notmyidea.org/using-jpype-to-bridge-python-and-java.html
 
# 
#import jpype
#import os
# 
## jvm.dll'nin yolu. Java versiyonu değişirse, alttaki satır güncellenecek.
#jvmDLLpath = r"C:\Program Files\Java\jdk1.7.0_79\jre\bin\server\jvm.dll"
# 
## CLASSPATH  ayarlaması. .jar  dosyalarının doğrudan path bilgisi ve .jar ismi burada belirtilecek.
#classpath = r"D:\Belgelerim\_JAVA_JARs_FOLDER\zemberek\zemberek-cekirdek-2.1.1.jar"
#classpath = os.pathsep.join((classpath, "D:\Belgelerim\_JAVA_JARs_FOLDER\zemberek\zemberek-tr-2.1.1.jar"))
# 
## JVM'nin BAŞLATILMASI
#jpype.startJVM(jvmDLLpath,"-ea", "-Djava.class.path=%s"%classpath)
# 
# 
# 
#################################################################################################
## .JAR'daki class'lardan object'lerin oluşturulması
# 
## [JAVA EŞDEĞERİ]  tt = new(net.zemberek.tr.yapi.TurkiyeTurkcesi())
#TT = jpype.JClass("net.zemberek.tr.yapi.TurkiyeTurkcesi")
#tt = TT()
# 
## [JAVA EŞDEĞERİ]  z = new(net.zemberek.erisim.Zemberek(tt))
#Z = jpype.JClass("net.zemberek.erisim.Zemberek")
#z = Z(tt)
#################################################################################################
# 
# 
#ornSonuc = z.kelimeCozumle("karadan")
#print(type(ornSonuc))
#print(ornSonuc[0])
#print(ornSonuc[0].kok().tip().toString())
#print(ornSonuc[0].kok().icerik())
# 
# 
# 
## JVM'nin SONLANDIRILMASI
#jpype.shutdownJVM()