# -*- coding: utf-8 -*-
"""
Created on Wed Jun 15 15:49:28 2016

@author: ayhanyavuz
"""

from nltk.stem.snowball import SnowballStemmer

print(" ".join(SnowballStemmer.languages))


stemmer = SnowballStemmer("english")
print(stemmer.stem("runnig"))
stemmer2 = SnowballStemmer("english" , ignore_stopwords=True)
print (stemmer.stem("having"))