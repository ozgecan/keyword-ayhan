# -*- coding: utf-8 -*-
"""
Created on Fri Jun 24 09:09:20 2016

@author: ayhanyavuz
"""


import nltk
import string
from nltk.corpus import stopwords
from collections import Counter
def get_tokens():
   with open('/home/ayhanyavuz/python scripts/keywordExtraction&Summarizing/TextRank-master/articles/10.txt', 'r') as articles:
    text = articles.read()
    lowers = text.lower()
    #remove the punctuation using the character deletion step of translate
    no_punctuation = lowers.translate(None, string.punctuation)
    tokens = nltk.word_tokenize(no_punctuation)
    return tokens


tokens = get_tokens()
filtered = [w for w in tokens if not w in stopwords.words('english')]
count = Counter(filtered)
print count.most_common(10)
