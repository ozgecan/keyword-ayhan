# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 09:17:03 2016

@author: ayhanyavuz
"""

import nltk, Zemberek
import string, os, io
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from snowballstemmer import TurkishStemmer
import networkx as nx
import itertools



path = 'data'
token_dict = {}

def filter_for_tags(tagged, tags=['NN', 'JJ', 'NNP']):
    return [item for item in tagged if item[1] in tags]
    
def normalize(tagged):
    return [(item[0].replace('.', ''), item[1]) for item in tagged]

def unique_everseen(iterable, key=None):
    "List unique elements, preserving order. Remember all elements ever seen."
    # unique_everseen('AAAABBBCCDAABBB') --> A B C D
    # unique_everseen('ABBCcAD', str.lower) --> A B C D
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in itertools.ifilterfalse(seen.__contains__, iterable):
            seen_add(element)
            yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield element
                
def lDistance(firstString, secondString):

    if len(firstString) > len(secondString):
        firstString, secondString = secondString, firstString
    distances = range(len(firstString) + 1)
    for index2, char2 in enumerate(secondString):
        newDistances = [index2 + 1]
        for index1, char1 in enumerate(firstString):
            if char1 == char2:
                newDistances.append(distances[index1])
            else:
                newDistances.append(1 + min((distances[index1], distances[index1+1], newDistances[-1])))
        distances = newDistances
    return distances[-1]

def buildGraph(nodes):
    "nodes - list of hashables that represents the nodes of the graph"
    gr = nx.Graph() #initialize an undirected graph
    gr.add_nodes_from(nodes)
    nodePairs = list(itertools.combinations(nodes, 2))
    #add edges to the graph (weighted by Levenshtein distance)  
    for pair in nodePairs:
        firstString = pair[0]
        secondString = pair[1]
        levDistance = lDistance(firstString, secondString)
        gr.add_edge(firstString, secondString, weight=levDistance)
               
    return gr

def tokenize(text):
    
    language='turkish'
    stopwords_= stopwords.words(language) 
    digit = ['0','1','2','3','4','5','6','7','8','9']
    stopwords_.extend(["bir","bu","iki","üç".decode("utf8"),"’", "’de", "’da"]) 
   
    tokens= nltk.word_tokenize(text, language='turkish')
    tokens=list(set(tokens)-set(stopwords_)-set(digit))
    stems = []
    for item in tokens:
        stems.append(TurkishStemmer().stemWord(item))
#    stems = sorted(stems,reverse=True) 
    
    return stems

def writeFiles(keyphrases, fileName):
    "outputs the keyphrases to appropriate files"
    print "Generating output to " + 'keywords/' + fileName
    keyphraseFile = io.open('keywords/' + fileName, 'w',encoding='utf8')
    for keyphrase in keyphrases:
        keyphraseFile.write(keyphrase + '\n')
    keyphraseFile.close()

for dirpath, dirs, files in os.walk(path):
    for f in files:
        fname = os.path.join(dirpath, f)
        print "fname=", fname
        with open(fname) as data:
            text = data.read()
            token_dict[f] = text.lower().translate(None, string.punctuation).decode("utf8")
    
    
tfidf = TfidfVectorizer(tokenizer=tokenize)
tfidf_matrix =  tfidf.fit_transform(token_dict.values())
feature_names = tfidf.get_feature_names() 

dense = tfidf_matrix.todense()
episode = dense[0].tolist()[0]
phrase_scores = [pair for pair in zip(range(0, len(episode)), episode) if pair[1] > 0]
sorted_phrase_scores = sorted(phrase_scores, key=lambda t: t[1] * -1, reverse=True)

limit=int(len(phrase_scores)-((len(phrase_scores)*20)/100))
filtered=[feature_names[word_id] for (word_id, score) in sorted_phrase_scores][:limit]
   
tagged=Zemberek.tr_pos_tag(filtered)
#tagger temizleme
textlist = [x[0] for x in tagged]
tagged = filter_for_tags(tagged)
tagged = normalize(tagged)

unique_word_set = unique_everseen([x[0] for x in tagged])
word_set_list = list(unique_word_set)
#this will be used to determine adjacent words in order to construct keyphrases with two words
graph = buildGraph(word_set_list)

#pageRank - initial value of 1.0, error tolerance of 0,0001, 
calculated_page_rank = nx.pagerank(graph, weight='weight')

#most important words in ascending order of importance
keyphrases = sorted(calculated_page_rank, 
                    key=calculated_page_rank.get, 
                    reverse=True)

#the number of keyphrases returned will be relative to 
#the size of the text (a third of the number of vertices)
aThird = len(word_set_list) /3
keyphrases = keyphrases[0:aThird+1]

#take keyphrases with multiple words into consideration 
#as done in the paper - if two words are adjacent in the 
#text and are selected as keywords, join them together
modifiedKeyphrases = set([])

#keeps track of individual keywords that have been joined 
#to form a keyphrase
dealtWith = set([]) 
i = 0
j = 1
while j < len(textlist):
    firstWord = textlist[i]
    secondWord = textlist[j]
    if firstWord in keyphrases and secondWord in keyphrases:
        keyphrase = firstWord + ' ' + secondWord
        modifiedKeyphrases.add(keyphrase)
        dealtWith.add(firstWord)
        dealtWith.add(secondWord)
    else:
        if firstWord in keyphrases and firstWord not in dealtWith: 
            modifiedKeyphrases.add(firstWord)

        #if this is the last word in the text, and it is a keyword,
        #it definitely has no chance of being a keyphrase at this point    
        if j == len(textlist)-1 and secondWord in keyphrases and secondWord not in dealtWith:
            modifiedKeyphrases.add(secondWord)
    
    i = i + 1
    j = j + 1
#print tagged
print modifiedKeyphrases  
writeFiles(modifiedKeyphrases,'4.txt')