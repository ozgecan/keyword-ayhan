# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 09:17:03 2016

@author: ayhanyavuz
"""

import nltk
import string, os
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from snowballstemmer import TurkishStemmer



path = 'articles'
token_dict = {}

def tokenize(text):
    
    language='turkish'
    stopwords_= stopwords.words(language) 
    digit = ['0','1','2','3','4','5','6','7','8','9']
    stopwords_.extend(["bir","bu","iki","üç".decode("utf8"),"’", "’de", "’da"]) 
   
    tokens= nltk.word_tokenize(text, language='turkish')
    tokens=list(set(tokens)-set(stopwords_)-set(digit))
    stems = []
    for item in tokens:
        stems.append(TurkishStemmer().stemWord(item))
    stems = sorted(stems,reverse=True) 
    
    return stems

for dirpath, dirs, files in os.walk(path):
    for f in files:
        fname = os.path.join(dirpath, f)
        print "fname=", fname
        with open(fname) as articles:
            text = articles.read()
            token_dict[f] = text.lower().translate(None, string.punctuation).decode("utf8")

tfidf = TfidfVectorizer(tokenizer=tokenize)
#tfs = tfidf.fit_transform(token_dict.values())
#idf = tfidf.idf_
#tfidf_voc = dict(zip(tfidf.get_feature_names(), idf))
#
#sorted_phrase_scores = sorted(tfidf_voc.values, key=lambda t: t[1] * -1)

tfidf_matrix =  tfidf.fit_transform(token_dict.values())
feature_names = tfidf.get_feature_names() 

dense = tfidf_matrix.todense()
episode = dense[0].tolist()[0]
phrase_scores = [pair for pair in zip(range(0, len(episode)), episode) if pair[1] > 0]
sorted_phrase_scores = sorted(phrase_scores, key=lambda t: t[1] * -1, reverse=True)

limit=int(len(phrase_scores)-((len(phrase_scores)*20)/100))
filtered=[feature_names[word_id] for (word_id, score) in sorted_phrase_scores][:limit]
   
#str = 'Tarihsel olarak en önemli eski hesaplama aleti abaküstür; 2000 yildan fazla süredir bilinmekte ve yaygın olarak kullanılmaktadır. Blaise Pascal, 1642’de dijital hesap makinesini yapmıştır; yalnızca tuşlar aracılığı yla girilen rakamları toplama ve çıkarma işlemi yapan bu aygıtı, vergi toplayıcısı olan babasına yardım etmek için geliştirmiştir.1671’de Gottfried Wilhelm Leibniz bir bilgisayar tasarlamıştır; 1694 yılında yapılabilen bu araç özel dişli mekanizması kullanmaktaydı; toplama, çıkartma, çarpma ve bölme işlemi yapabiliyordu.'.decode("utf8")
#response = tfidf.transform([str])
#print response
#
#feature_names = tfidf.get_feature_names()
#for col in response.nonzero()[1]:    
#        print feature_names[col], ' - ', response[0, col] 
#        
