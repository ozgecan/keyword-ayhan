# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 09:47:08 2016

@author: ayhanyavuz
"""

import nltk, string
from nltk.corpus import stopwords
#from collections import Counter

def get_tokens():
   with open('1.txt', 'r') as shakes:
    text = shakes.read()
    lowers = text.lower()
    #remove the punctuation using the character deletion step of translate    
    no_digits = lowers.translate(None, string.digits)
    no_punctuation = no_digits.translate(None, string.punctuation)
    
    tokens = nltk.word_tokenize(no_punctuation.decode("utf8"))
    return tokens

tokens = get_tokens()
stopwords_= stopwords.words('turkish') + ["bir","iki","üç".decode("utf8"),"’", "’de", "’da"]
filtered = [w for w in tokens if not w in stopwords_]