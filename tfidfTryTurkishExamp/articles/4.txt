Eyalet ve Beylerbeyliği
Eyalet Sistemi
Bir beylerbeyliğinin yönetiminde en büyük idari-askeri birim olan eyalet uzun bir gelişme ve değişme
çizgisine sahip olmuştur. Osmanlı devletinde ilk olarak Rumeli eyaleti teşkil edilmiştir. Rumeli’de gerçekleştirilen
büyük fetihlerden sonra 1. Murat bu toprakların idaresini lala şahin paşaya bırakmıştır. Böylece takriben 1362 de
Rumeli beylerbeyliği daha ziyade askeri bir özellikle ortaya çıkmıştır. İlk beylerbeylik olan Rumeli bu imtiyazlı
statüsünü devletin inkırazına kadar korumuştur.
Yıldırım Bayezid 1393te Rumeli’ye geçerken kara Timurtaş paşayı Anadolu beylerbeyi olarak Ankara’da
bırakmıştır. Böylece askeri bir zorunluluk olarak ikinci bir beylerbeyliği Anadolu beylerbeyliği doğmuştur. Fetret
devrini takriben 1413te 3. Olarak Amasya ve Sivas merkez olmak üzere “Rum beylerbeyliği” teşkil edilmiştir.
Osmanlı devleri uzun süre bu üç eyaletle idare edilmiştir. Daha sonra Fatih devrinde karaman eyalet
statüsüne dönüştürüldü. Yavuz Sultan Selim devrinde ise Diyarbakır Halep Şam eyaletleri teşkil edilmiştir.
Kanuninin uzun saltanatı sırasında Dulkadir, Cezayir-i bahri sefid, Cezayir-i garp, Musul Erzurum Bağdat
Yemen Budin Basra Van Timişvar Lahze Trablusgarp Habeş olmak üzere 14 yeni eyalet teşkil edilmiştir.
II. Selim Devrinde ise Kırımda Kefe Kıbrıs Tunus ve Trablusşam olmak üzere 4 eyaletin daha kurulduğu
III. Murat Devrine gelindiğinde 25 eyaletin teşkil edildi. III. Murat saltanatının başında; Çıldır, Trabzon, Bosna ve
Kars eyaletleri teşkil edilmiş bir süre sonra İran savaşları sebebiyle bu hükümdar zamanında doğuda birçok yapay
eyaletlerin teşkil edildiği ancak bunlar kalıcı olmayıp bir süre sonra elden çıktığı yada gereksiz olduğu için
kaldırıldığı görülmektedir.
Osmanlı eyaleti statü bakımından TIMARLI ve SALYANELİ olmak üzere 2’ye ayrılırdı. Tımar sisteminin takip
edildiği eyaletlere Tımarlı eyaletler eyalet, gelirinin tımar olarak dağıtılmayıp tamamının devlet namına toplanıp bu
toplamdan askerin ve eyalet idaresinin ücretlerini ödedikten sonra geri kalanının devlet hazinesine gönderildiği
eyaletlere ise saliyaneli denirdi.. 1609 tarihli Ayn Ali Efendi risalesine göre timar sisteminin uygulandığı normal
eyaletler 23, salyaneli eyalet sayısı da Mısır, Yemen, Habeş Bağdat, Barsa, Lahza, Trablusgarp, Tunus ve Cezayir-i
garp olmak üzere 9 idi..
Eyaletin idaresi
İdaresinin ücretlerini Bir eyalet, sancak beyinin idaresi altındaki sancak veya liva denilen idari birimlerden meydana
geliyordu. Sacak temel idare birimiydi ve Beylerbeyinin bizzat kendisi paşa sancağı denilen merkez sancakta
bulunuyordu. Beylerbeyi hükümdarın icra gücünün temsilcisi olarak eyaletin bütün işlerinden sorumlu ve en yetkili
olurlar, vali sıfatı ile anılırlar idi.
Fakat bir eyaletteki kadı ve taşrada hazire çıkarlarını temsil eden mal ( hazine) defterdarı kararlarından beylerbeyine
bağlı olmadığı gibi doğrudan doğruya merkezi hükümete karşı sorumluydular. Ayrıca eyalette muayyen bölgelere
yerleştirilmiş yeniçeri garnizonları da beylerbeyine bağlı değildi. Hatta bu garnizonların bulunduğu kalelere
girmeleri yasaktı. Bu birlikler yalnız hükümdarın emri ile eyleme geçmekteydiler.
Şüphesiz bütün bunlar eyaletlerde beylerbeyinin fazla güçlenmesini önlemek ve merkezi idarenin nüfuzunu
korumak düşüncesinden kaynaklanmaktaydı.
Eyalet temel olarak timar sistemi çerçevesinde teşkil edilmişti ve beylerbeyi öncelikle timarlı sipahi
ordusundan sorumluydu. Onun emri altındaki eyalet kuvvetleri devlet ordusunun büyük bir askeri ünitesini
oluştururdu.
TARIHVEMEDENIYET.ORG
Beylerbeyi [1530 sonrası bir miktara kadar] timar tevcih hakkına sahipti (bunlara tezkeresiz timar denir ) ve
idaresi aldın ki defterdar kethüdası, timar defterdarı bu işlerle uğraşırdı. Timarların kaydedildiği kütük defterleri ve
her sancak için ayrı ayrı hazırlanan mufassal ve icmal defterlerinin bir kopyası eyalette de bulunurdu.
Eyalet ( Beylerbeyi ) Divanı
Eyalet idaresinde en yetkili organ olan ve doğrudan doğruya beylerbeyine bağlı olarak onun başkanlığından
toplanan bu divanın ne zaman kurulduğu bilinmemektedir. Ancak Selçuklulardaki benzerleri düşünülecek olursa
bir danışma kurulu olarak ilk eyaletin teşkilatından beri mevcut olduğu düşünülebilir.
Divan eyaletin merkezi paşa sancağında beylerbeyinin konağında( Paşa sarayı )toplanırdı. Buraya divanhane
denilirdi. Divanı hümayun ülke idaresinde en yetkili kurum olduğu gibi bu divanda eyalet idaresinin temel kurulu
ve yapı itbarıyla divanı hümayunun küçük bir modeliydi.
Divanda beylerin başkanlığında,
hazineye ait gelir kaynaklarını yöneten hazine defterdarı,
timar işlerine bakan timar defterdarı,
timar ve zeametlerle ilgili işleri düzenleyen defter kethüdası,
hukuki işlere bakan eyalet kadısı, divan efendisi, tezkereci, çavuşlar, ruznameciler ve kâtipler bulunurdu.
Türkçe bilinmeyen halkın bulunduğu Arap ve balkan eyaletlerinden halkın talep ve şikâyetlerini Türkçeye
çevirmesi divan kararlarının ise halkın diline tercümesi için mahalli dilleri bilen tercümanlar divanda istihdam
edilirdi. Sekreterya (raportör) işlerini ise divan efendisi başkanlığında katipler yürütürdü.
Kadının divan üyesi olup olmadığına dair ise her iki yönde de görüşler vardır.
Eyalet Divanında Görüşülen Konular
Eyalet divanı halkın dile ve şikâyetlerine açıktı. Dolayısıyla halkın verdiği arzuhal ve mahzarlar veya şifahi
müracaatlar bu divan gönderiminin önemli bir kısmını teşkil eder. En başta gelen konuları timar meselesi
oluşturuyordu. Eyalet divanının kararlarından memnun olmayan şikâyetlerini divanı hümayuna bildirirdi. Temel
konular eyalet divanında görüşüldükten sonra şer’i meseleler kadı divanına mali konular defterdar dairesine havale
edilirdi. Eyalet divanında görüşülen konular ve varılan kararlar deftere kayıt edilirdi. Ayrıca pek çok konuda divanı
hümayun’un onayı alınırdı
Osmanlı eyaletinin sınırları ve bilhassa statüsünden ve yönetim kararlarında zamanla önemli değişikliler
olmuş nihayet 1864te “vilayet nizamnamesi” il batı usulü çok farklı bir statü benimsenmiştir.
Beylerbeyi
Eyaletin en büyük amiri olan beylerbeyi için Osmanlı kaynaklarında mir-i miran, emir-ül ümera ve 18.
Asırdan itibaren vali denilmiştir. Osmanlı teşkilatınca önceleri geniş askeri yetkilere sahip kumandan anlamında
kullanılırken yeni fetihlerden sonra kurulan eyaletlerle birlikte beylerbeyleri idari ve askeri yetkilere sahip oldular.
Ayrıca 15. asır da beylerbeylik ve özellikle Rumeli beylerbeyliği rütbe ve paye olarak kullanılmıştır.
TARIHVEMEDENIYET.ORG
Beylerbeyinin Seçimi
Osmanlıda 16. Asırdan itibaren seyfiye (kılıç) ilmiye, kalemiyye adıyla meslekler belirli hale gelmiş
beylerbeyliği seyfiye içerisinde yer almıştır. Kuruluş döneminde genellikle Türk kumandanları beylerbeyi olarak
tayin edilirken fatih devrinde beylerbeyliği devşirmelere verilir olmuştur. Acemi oğlanlar mektebi ve ardından
Enderun’dan yetişenler saraydan taşraya çıktıklarında çeşitli hizmetlerde bulunuyor ve taşra teşkilatından en yüksek
göreve beylerbeyliğine kadar yükseliyordu. Sarayda kapı ağalığından ve yeniçeri ağalığından taşraya beylerbeyi
olarak çıkabiliyorlardı. Teşkilatın diğer kademelerinden de geçmek mümkündü. Fatih kanunnamesine göre 4
yoldan beylerbeyi olunabiliyordu. Mal defterdarları, Beylik ve nişancı oğlanları 500 akçe kadılarının ve 400,00
akçeye varmış sancak beyleri. Aynı kanunnamede padişah kızlarının oğullarına beylerbeyliği verilmiyor sancak
beyliği verilmesi belirlenmektedir.
Beylerbeyinin Atanması
Beylerbeyliğine tayin edilenlere berat (menşur) verilirdi. Beylerbeyi ve sancak beyi beratları divanı hümayun
tahvil kaleminde hazırlanır bunun içinde berat haracı alınırdı. Vezaret rütbesiyle tayin edilenlere genellikle menşur
verilirdi. Tek tek olduğu gibi toplu tayinlerde olurdu. Osmanlı vakayinameleri toplu tayinleri ayrıntılı olarak
vermektedir. Kendilerine eyalet verilen beylerbeyleri divanı hümayunda hil’at giyer ve arza giderek yer öperdi.
Beylerbeyinin Görev Süresi ve Azli
Beylerbeyinin görev süresi diğer mesleklerle olduğu gibi oldukça uzundu. 16. Asır 2. Yarısından itibaren
çeşitli nedenlerle süre kısalmıştır. 16. Ve 17. Asırlara ait sancak tevcih defterlerinde ve ayrıca sicil-i Osmanî de
beylerbeyinin görev süresi hakkında fikir edinmek mümkün olmaktadır. Sürenin genellikle 1 yıl civarında olduğu
söylenebilir. Ancak özel durumu olan bazı eyaletler de bu süre daha uzun olabilirdi. Nitekim Yemenli Hasan Paşa
aralıksız 24 sene yemen valiliğinde bulunmuştur.
Kaynaklarda beylerbeyliği konusunda yapılan en sert eleştirenlerin başında sürenin kısalığı gelmektedir.
Kuruluş yıllarında uzun süre görevde kalmasına karşılık 17. Asırdan itibaren beylerbeyinin sık sık değişmesi hem
bu kişileri hem de halkı tedirgin etmiştir. 17. Ve 18. Asırlarda beylerbeyi 1 yılda 2-3 kere bir eyaletten başka bir
eyalete tayin edilir olmuşlardır. Sık sık azledilen beylerbeyleri büyük yekûn yol masraflarını halktan almak istediler.
3. Selim bunları önlemek için bir hattı hümayun çıkarmıştır. Bu konuda 18. Asrın başlarında çıkarılan bir ferman
münasebetiyle tarihçi raşid yaptığı değerlendirmede şu hususlara temas etmektedir; seferin uzaması ve peş peşe
olması beylerbeyi sayısını lüzumsuz yere artırmış bunlara Rumeli ve Anadolu da bulunan sancaklar bile kâfi gelmez
olmuşlardır. Beylerbeyi nöbetleşe tayin edilir mazur olanlar ise zaruret içerisinde çaresiz kalmışlardır. İstanbul’a
gelen mazur beyleri ileri gelen devlet adamlarının konaklarını dolaşarak yardım ister duruma düşmüşlerdir.
Bunlardan kabiliyetli ve layık olanlar eyaletlere diğerleri de bazı sancaklara tayin edilmiş veya bir miktar emekli
ulufesi bağlanarak kontrol altına alınmıştır. Bundan sonra beylerbeylerinin artık izinsiz istanbul’a gelmeleri
önlenmiştir. Mazur olan beylerbeylerinin 2. Bir göreve kadar geçirdikleri zamana mülezamat denirdi.
17. Asır ortalarında sunulan bir telhis de Osmanlı padişahlarının ülkeyi beylerbeyleri ve sancak beyleri ile
idare ettikleri bu kişilerin azledilmemeleri beylerbeylerinin süresiz olarak tayin edilmeleri zaruri bir sebep olmadıkça
görevden alınmamaları teklif edilmektedir.
Beylerbeyinin Görev ve Yetkileri
Beylerbeyinin görevleri barış ve savaş dönemleri olmak üzere başlıca iki devirde incelemek gerekir.
Görevleri ana hatlarıyla;
TARIHVEMEDENIYET.ORG
 Reayanın korunması
 Askeri nizamın sağlanması
 Zulmün bertaraf edilmesi
 Eyaletin idaresi
 Seferlere iştirak şeklinde eyaletteki sancak beylerinin kadıların ve diğer idarecilerinin ona tabi olmaları
eğer vezareti varsa çevresindeki beylerbeylerinin ona itaat etmesi gerektiği bildirilmektedir.
Bazı beylerbeyleri geniş yetkilere sahip olduğu bilinmektedir. En önemli görevlerinden biriside Timarların
verilmesidir. Kendi eyaletinde vekil-i saltanat olarak bütün timar sipahilerinin ve askerlerin amiri olması sebebiyle
timar tevcihleri bu konuda çeşitli itilafların halli beylerbeyleri ve eyalet divanlarını en çok meşgul eden hususlardır.
Başlangıçta bütün timarlar beylerbeyliği tarafından tevcih edilirken 1530dan itibaren sadece küçük timarlar tevcih
edilir olmuşlardır. Eyalette güvenliğin sağlanması da beylerbeylerinin önemli görevlerindendi. Bu konuda çok sert
tedbirler almak görevliler tayin etmek yetkisi vardı. Eyalet halkından zulme yada haksızlığa uğrayan kimseler bizzat
beylerbeyliğine veya eyalet divanına topluca mazhar veya tek kişilik arzu haller sunarlardı. Taraflar beylerbeyi
buyurdusu ile eyalet divanından muhakeme edilebilirdi. Ehli örften olan beylerbeyi ehli şerden olan eyalet kadısı
müftüsü ve tayinleri arasında oldukça hassas bir denge vardır. Bunlardan birsinin doğrudan saltanat makamına
veya divanı hümayuna arzda bulunma yetkisinin olması beylerbeyinin ölçülü ve adil davranmasında çok etkili
olmuştur.
Eyalet kadılarının beylerbeyi aleyhinde gönderdikleri pek çok arz ve ariza örneği bulunmaktadır. Bazen
bölgedeki kadıların ortak şikâyetleri üzerine beylerbeyinin azledildiği cezalandırıldığı malının da elinde alındığı
olurdu. Ancak beylerbeyi statü bakımından kadı müftü ve diğer ilmiye adamlarından daha üstündü.
Beylerbeyinin Seferdeki Görevleri
Beylerbeylerinin bulundukları bölgenin en büyük askeri amiri ve kumandanı durumunda olmaları onların
yetki ve sorumluluklarının hayli artırmıştır. Hemen her dönemde kısa aralıklarla çok cepheli savaşların yapılması
beylerbeylerinin meslek hayatlarının yaklaşık 3/2 sini cephede fiilen muharebe ederek veya bunun hazırlıklarıyla
meşgul olarak geçirmek zorunda bırakmıştır.
Beylerbeyleri sefer ve sefer hazırlıklarıyla ilgili fermanlar gönderilir sancak beyleri sipahiler kendi kapı
halkıyla yoğun bir hazırlık içerisine girerlerdi. Bölgesinde vekil-i saltanat olan beylerbeyleri sefere çıktıkları zaman
ulema sadaat eşraf ve halkın katıldığı büyük bir törenle uğurlanırlardı. Seferin her safhasında fiilen görevi üstlenirler
Serdar-ı Ekrem ile sık sık müşaveret yapıp toplanan divanlara katılarak strateji tespitinde yardımcı olurlardır.
Beylerbeylerinin serdar olarak tayin edildiği de olurdu. Seferlerde beylerbeylerinin karargâh ve cephedeki faaliyetleri
hakkında Osmanlı kaynaklarında oldukça ayrıntılı bilgiler bulunmaktadır. Diğer taraftan beylerbeyleri sancak
beyleri gerek sefere hazırlık gerekse savaşın seyri sırasındaki ihmal hata ve beceriksizliklerinin cezasının çok ağır
şekilde öderler, bazen hayatlarına bile mal olurdu. Beylerbeylerinin ölümünden sonra geride bıraktığı malları belli
ölçüde devlet tarafından müsadere edilirdi.
Rumeli beylerbeyinin özelliği
Rumeli beylerbeyliğinin ilk beylerbeyliği olması ve Rumeli’nin dar-ul cihad olarak üstünlüğü sebebiyle
diğerlerinden farklıydı. Her hangi bir iş için İstanbul’da bulunursa divan toplantısına katılması 1536 dan itibaren
adet olmuştur. Vezir-i azamların sadarete idareten Rumeli beylerbeyliğini üstlendiği de olurdu. Nitekim fatih
döneminde Vezir-i azam Mahmud paşa kanuni döneminde İbrahim Paşa ayın zamanda Rumeli beylerbeyi idiler.
Diğer taraftan Mısır Budin Şam Bağdat Habeş Lahza Yemen gibi önemli eyaletlerin beylerbeyi de vezaret payesine
sahip idiler ve kendilerine icraat ve teşrifatta bazı imtiyazlar tanınırdı
TARIHVEMEDENIYET.ORG
Beylerbeyliğinin Gelir ve Giderleri
Beylerbeyinin gelir ve giderlerini görev ve mazuliyet dönemleri olarak iki safhada incelemek mümkündür.
Görevde bulundukları sırada gelirlerinin başında timar hasılatı gelmektedir. Fatih kanunnamesinde beylerbeyi
hasların 800.000 ile 1.200.000 akçe arasında olabileceği işaret edilmiştir. Beylerbeylerinin has olarak tahsis edilen bu
miktarın dışında pek çok kaynaktan geliri vardır. Buna karşılık kendine ait şahsi harcamaları kapı halkı masrafları
çeşitli kimselere verdikleri hediyeler ve caizeler çok büyük rakamlara ulaşmaktadır.ancak
17. Asırdan itibaren başta fiyat artışları olmak üzere çeşitli sebeplerle artan masraflarını salgın, salma,
imdad-ı seferiye gibi yollarla kısmen de olsa halktan karşılamak istemişler. Buysa halkın kitleler halinde mazhar ve
arzuhalle şikâyetlerine sebep olmuştur. Devlet zulüm ve biat dediği bu davranışları şiddetle yasaklamış,
müfettişlerle bunları önlemek istemiştir. Bu konuda yapılan teftişler azledilen beylerbeyi hakkında Osmanlı
kaynaklarında pek çok bilgi bulunmaktadır.
Beylerbeylerinin Mazuliyet Dönemindeki Gelirleri
Beylerbeylerinin mazuliyet dönemindeki gelirleri ve ona nispetle giderleri son derece sınırlıydı. Bu
dönemde belirli bir miktar yevmiye ve arpalık tahsis edilirdi. Fatih kanunnamesinde 100.000 akçe emekli ücreti
alacakları veya asafname’de 150 akçe emekli ücreti verileceği veya zeamet ile olursa yıllık 80.000 akçe tahsis
edileceği görülmektedir.
Özel Statülü Eyaletler
Osmanlı devleti Anadolu Rumeli ve Arabistan olmak üzere başlıca 3 ana bölgeden oluşturulmaktadır. Buralarda
temel idari birim eyalettir. Ancak 3 kıtada geniş topraklara sahip olan Osmanlı devletinde eyalet dışına özel statüsü
olan yerler bulunmaktadır.
1 Mekke ve Medine(Haremeyn) : Eyalet statüsüyle doğrudan merkeze bağlı olmayıp emirlik emaret olarak H.z
peygamber soyundan gelen şerifler tarafından idare edilmekteydi
2. Kırım: Osmanlıya bağlı bir hanlık olarak giray hanedanlığı tarafından yönetilmekteydi
3 Afrika: eyalet statüsünde olan Tunus ve Cezayir ise önceleri beylerbeylik daha sonrada Tunus; beylerbeylik
Cezayir de: dayı’lar tarafından idare edilmiştir.
4 Eflak –Boğdan, Erdel ; Voyvodalık ile idare edilmekte olan stratejik öneme sahip bu yerlerin yöneticileri
topladıkları voyvodalık divanı ile kendi bölgelerini idare eder ve buralarda dava dinlenir mali idari kararlar alınır
buraların yöneticileri halkın güvendiği yerli Hıristiyan ailelerden seçilirdi.
5 Dubrovnik ( Ragus ) ise Osmanlı devletine yıllık vergi ödeyen bir cumhuriyet idi.
Sancak ve Sancakbeyliği
Selçuklu hükümdarı III. Alâeddin, Osman gaziye çeşitli hediyeler ve sancak göndermiş, böylece onu
bölgesinde sancak beyi nasb etmişti. ,ilk Osmanlı sancak beyi ise Orhan gazinin oğlu Süleyman paşa ve Murat
gaziydi.
İmparatorluğun temel yönetim birimi sancak idi. Birkaç sancak birleşerek eyaleti oluştururdu. Eyaletin
merkezi doğrudan beylerbeyinin idaresindeki paşa sancağıydı. Diğer sancaklara merkezden atama yapılırdı.
TARIHVEMEDENIYET.ORG
16. yy sonlarına kadar şehzadelerin lala eşliğinde tecrübe kazanmak için görev yaptıkları sancaklara, “ çelebi
sultan sancağı “ denirdi. .Fatih devrinde, Manisa, Amasya, Konya bunlardandır.
16. Ve 19. Yüz yıllarda sancak ve sancak teşkilatında önemli değişiklikler olmuştur. 16. Asır sonlarında 30-
32 eyalette 500 kadar 19. Yüzyılın sonlarında ise 25 eyalette 290 kadar sancağın olduğu görülmüştür. Tanzimattan
sonraki idari taksimatta ise devlet salnamelerinde takip etmek mümkündür.
Sancak beylerinin tayini ve görev süreleri
Sancaklara merkezden; Birun ve Enderun halkından, müteferrikalarından kapı kullarından birisinin bey
tayin edildiği veya taşradan alaybeyi, defter kethudası, timar veya hazine defterdarlarından tayin edilirdi.
Bunların zamanla nispetlerinde değişmeler olduğu görülmektedir. Sancak beylerinin genellikle 1 yıl ile 3 yıl
arasında görev yaptıkları mazuriyet dönemlerinin de 2 yıl gibi kısa sürdüğü sonrada tekrar diğer bir sancağa veya
beylerbeyliğine geçtikleri bilinmektedir. Yeniden bir sancağa tayin edilirken bu görev aynı beylerbeyliği içinde veya
başka bir eyalette olabilmektedir.
Hükümet Sistemi
Doğu anadoluda aşiret boylarının kuvvetli olduğu yerlerde özellikle Diyarbakır eyaleti bünyesnide hükümet
terimiyle ifade edilen özel statüye sahip snacaklar vardı. Bunların beylerine “hakim” denilirdi ve bunlarda idare
ırsen babadan oğla intikal ederdi. Hükümetle idare edilen yerlerde hakimler vergileri toplar sancağın ihtiyacı olan
kısmı sarfedildikten sonra geri kalanı merkeze gönderilirdi. Buna karşılık belirlenen asker ile sefere katılmaları
gerekirdi. Merkezi idare bir denge ve kontrol unsuru oalrak bu yerlere merkezden kadılara tayin etmiş ve yeniçeri
birlikleri göndermiştir. Ancak buralarda tahrir yapılmaz bunun sonucu olarak da timar sistemi uygulanmazdı.
Yurtluk – Ocaklık
Osmanlı devleti doğu Anadolu yöresinin idareye katılışı sırasında kendi saflarında yer alan mahalli beylere
istimaletname denilen (itaat karşısında hükümranlığın korunacağına dair ) belge vererek onları bulundukları kale,
köy kasabaların hakimi olarak tanımışlardır. Bununla beraber bu beyler seferlere düzenli olarak katılırlar ve
üzerlerine düşen verdileri öderlerdi. Bu düzene yurtluk ocaklık denilmiştir.
Bu sancaklardaki beylerin azli beylerbeyinin teklifi ile divan-ı hümayun tarafından yapılmaktaydı. Bu fertlerde
diğerlerinin tek farkı atamaların aynı aile bireyleri arasında yapılıyor olmasıydı.
Kaza İdaresi
Sancakların alt birimlerini kazalar oluştururdu. Kazalarda merkezden atanan devletin be aynı zamanda
halkın temsilcisi durumunda olan kadılar bulunurdu. Sancaklarda kaç kaza var ise o kadar da kadı olurdu. Kadılar
eyalet merkezi ile halk arasında bir köprü vazifesi görürlerdi. Yolların açılması ve onarımı , narh tesbiti , noterlik
hizmeti , adli idari mali denetim gibi görevleri yanı sıra en yüksek adli makam idiler. Yurtluk –ocaklık ile hükümet
sancaklarında da kadılar bulunurdu. 
