# -*- coding: utf-8 -*-
"""
Created on Mon Jun 27 14:44:27 2016

@author: ayhanyavuz
"""

import nltk
import string, os
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from snowballstemmer import TurkishStemmer



path = 'articles'
token_dict = {}

def tokenize(text):
    
    language='turkish'
    stopwords_= stopwords.words(language) 
    
    stopwords_.extend(["bir","iki","üç".decode("utf8"),"’", "’de", "’da"]) 
   
    tokens= nltk.word_tokenize(text, language='turkish')
    tokens=list(set(tokens)-set(stopwords_))
    stems = []
    for item in tokens:
        stems.append(TurkishStemmer().stemWord(item))
    stems = sorted(stems,reverse=True) 
    return stems

for dirpath, dirs, files in os.walk(path):
    for f in files:
        fname = os.path.join(dirpath, f)
        with open(fname) as articles:
            text = articles.read()
            token_dict[f] = text.lower().translate(None, string.punctuation).decode("utf8")

tf = TfidfVectorizer(tokenize, ngram_range=(1,3), min_df =0)
tfidf_matrix =  tf.fit_transform(token_dict.values())
feature_names = tf.get_feature_names()
#print tfidf_matrix
dense = tfidf_matrix.todense()
#print len(dense[0].tolist()[0])
articles = dense[0].tolist()[0]
phrase_scores = [pair for pair in zip(range(0, len(articles)), articles ) if pair[1] > 0]


#str = 'Tarihsel olarak en önemli eski hesaplama aleti abaküstür; 2000 yildan fazla süredir bilinmekte ve yaygın olarak kullanılmaktadır. Blaise Pascal, 1642’de dijital hesap makinesini yapmıştır; yalnızca tuşlar aracılığı yla girilen rakamları toplama ve çıkarma işlemi yapan bu aygıtı, vergi toplayıcısı olan babasına yardım etmek için geliştirmiştir.1671’de Gottfried Wilhelm Leibniz bir bilgisayar tasarlamıştır; 1694 yılında yapılabilen bu araç özel dişli mekanizması kullanmaktaydı; toplama, çıkartma, çarpma ve bölme işlemi yapabiliyordu.'.decode("utf8")
#response = tf.transform([str])
##print response
phrase_scores = [pair for pair in zip(range(0, len(articles)), articles ) if pair[1] > 0]
sorted(phrase_scores, key=lambda t: t[1] * -1)[:5]
#print sorted(phrase_scores, key=lambda t: t[1] * -1)[:5]
sorted_phrase_scores = sorted(phrase_scores, key=lambda t: t[1] * -1)
#print sorted_phrase_scores
limit=int(len(phrase_scores)-((len(phrase_scores)*20)/100))
for phrase, score in [(feature_names[word_id], score) for (word_id, score) in sorted_phrase_scores][:limit]:
   print('{0: <20} {1}'.format(phrase, score))

#print feature_names[190]
#for col in response.nonzero()[1]:    
#        print feature_names[col], ' - ', response[0, col] 
#        